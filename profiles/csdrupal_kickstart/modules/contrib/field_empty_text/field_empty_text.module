<?php
/**
 * @file
 * Contains the functionality for field_empty_text.
 */

/**
 * Implements hook_field_attach_view_alter().
 *
 * Show titles of empty fields if they are configured to display.
 */
function field_empty_text_field_attach_view_alter(&$output, $context) {
  $entity = $context['entity'];
  $entity_type = $context['entity_type'];
  $view_mode = $context['view_mode'];
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

  $instances = field_info_instances($entity_type, $bundle);
  foreach ($instances as $field_id => $instance) {
    // Set instance_display based on view_mode if it exists.
    $instance_display = isset($instance['display'][$view_mode]) ? $instance['display'][$view_mode] : $instance['display']['default'];

    if ($instance_display['type'] != 'hidden' && !empty($instance['field_empty_text']) && !isset($output[$field_id])) {
      // Load the field from the view mode, provide translatable empty text.
      $empty_text = t($instance['field_empty_text']);

      // Support token replacement if token is enabled.
      if (module_exists('token')) {
        $args = array($entity_type => $entity);
        $empty_text = token_replace($empty_text, $args, array('clear' => TRUE));
      }

      $empty_text = field_filter_xss($empty_text);

      // Add a render array to output for the field formatter to display.
      $output[$field_id] = array(
        '#weight' => $instance_display['weight'],
        '#label_display' => $instance_display['label'],
        '#title' => $instance['label'],
        '#bundle' => $bundle,
        '#entity_type' =>  $entity_type,
        '#field_name' => $field_id,
        '#field_type' => 'text',
        '#items' => array(),
        '#formatter' => 'text_default',
        '#theme' => 'field',
        '#items' => array(
          0 => array(
            'value' => $empty_text,
          ),
        ),
        0 => array('#markup' => $empty_text),
      );
    }
  }
}

/**
 * Implments hook_form_FORM_ID_alter().
 *
 * The specific alter hook for the field ui edit form.
 */
function field_empty_text_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  $form['instance']['field_empty_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Empty Text'),
    '#description' => t('Enter text to be displayed if this field is empty and configured to display.'),
    '#default_value' => isset($form['#instance']['field_empty_text']) ? $form['#instance']['field_empty_text'] : '',
  );
}
