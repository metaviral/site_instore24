<?php
/**
 * @file
 * csdrupal_amazon_parser.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function csdrupal_amazon_parser_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'clear csd_amazon_importer feeds'.
  $permissions['clear csd_amazon_importer feeds'] = array(
    'name' => 'clear csd_amazon_importer feeds',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'feeds',
  );

  // Exported permission: 'create csd_amazon_import content'.
  $permissions['create csd_amazon_import content'] = array(
    'name' => 'create csd_amazon_import content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any csd_amazon_import content'.
  $permissions['delete any csd_amazon_import content'] = array(
    'name' => 'delete any csd_amazon_import content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own csd_amazon_import content'.
  $permissions['delete own csd_amazon_import content'] = array(
    'name' => 'delete own csd_amazon_import content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any csd_amazon_import content'.
  $permissions['edit any csd_amazon_import content'] = array(
    'name' => 'edit any csd_amazon_import content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own csd_amazon_import content'.
  $permissions['edit own csd_amazon_import content'] = array(
    'name' => 'edit own csd_amazon_import content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'import csd_amazon_importer feeds'.
  $permissions['import csd_amazon_importer feeds'] = array(
    'name' => 'import csd_amazon_importer feeds',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'feeds',
  );

  // Exported permission: 'tamper csd_amazon_importer'.
  $permissions['tamper csd_amazon_importer'] = array(
    'name' => 'tamper csd_amazon_importer',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'feeds_tamper',
  );

  // Exported permission: 'unlock csd_amazon_importer feeds'.
  $permissions['unlock csd_amazon_importer feeds'] = array(
    'name' => 'unlock csd_amazon_importer feeds',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'feeds',
  );

  return $permissions;
}
