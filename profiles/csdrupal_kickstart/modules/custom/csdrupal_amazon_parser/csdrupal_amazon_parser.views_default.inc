<?php
/**
 * @file
 * csdrupal_amazon_parser.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function csdrupal_amazon_parser_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'csd_category_importers';
  $view->description = '';
  $view->tag = 'csdrupal';
  $view->base_table = 'node';
  $view->human_name = 'CS Drupal: Category importers';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'import csd_amazon_importer feeds';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'semanticviews_default';
  $handler->display->display_options['style_options']['list']['element_type'] = 'ul';
  $handler->display->display_options['style_options']['row']['element_type'] = 'li';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Content: Category (field_csd_category_ref) */
  $handler->display->display_options['relationships']['field_csd_category_ref_tid']['id'] = 'field_csd_category_ref_tid';
  $handler->display->display_options['relationships']['field_csd_category_ref_tid']['table'] = 'field_data_field_csd_category_ref';
  $handler->display->display_options['relationships']['field_csd_category_ref_tid']['field'] = 'field_csd_category_ref_tid';
  $handler->display->display_options['relationships']['field_csd_category_ref_tid']['required'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = '';
  $handler->display->display_options['fields']['edit_node']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['edit_node']['alter']['text'] = 'Edit importer node: [title]';
  $handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Category (field_csd_category_ref) */
  $handler->display->display_options['arguments']['field_csd_category_ref_tid']['id'] = 'field_csd_category_ref_tid';
  $handler->display->display_options['arguments']['field_csd_category_ref_tid']['table'] = 'field_data_field_csd_category_ref';
  $handler->display->display_options['arguments']['field_csd_category_ref_tid']['field'] = 'field_csd_category_ref_tid';
  $handler->display->display_options['arguments']['field_csd_category_ref_tid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['field_csd_category_ref_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_csd_category_ref_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_csd_category_ref_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_csd_category_ref_tid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'csd_amazon_import' => 'csd_amazon_import',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $translatables['csd_category_importers'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('term from field_csd_category_ref'),
    t('Edit importer node: [title]'),
    t('All'),
    t('Block'),
  );
  $export['csd_category_importers'] = $view;

  return $export;
}
