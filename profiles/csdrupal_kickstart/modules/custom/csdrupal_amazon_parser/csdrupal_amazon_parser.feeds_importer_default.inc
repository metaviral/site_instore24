<?php
/**
 * @file
 * csdrupal_amazon_parser.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function csdrupal_amazon_parser_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'csd_amazon_importer';
  $feeds_importer->config = array(
    'name' => 'CS Drupal: Amazon importer',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsXPathCrawler',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'num_pages' => '5',
        'first_run' => '1',
        'delay' => '1',
        'auto' => FALSE,
        'allow_override' => '1',
        'crawled' => FALSE,
        'xpath' => '//a[@id="pagnNextLink"]/@href',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserHTML',
      'config' => array(
        'sources' => array(
          'xpathparser:0' => './h3',
          'xpathparser:1' => '',
          'xpathparser:2' => './div[@class="image imageContainer"]/a//img/@src',
          'xpathparser:3' => './h3/a/@href',
          'xpathparser:4' => '',
          'xpathparser:5' => '',
          'xpathparser:6' => '',
          'xpathparser:7' => '',
          'xpathparser:8' => '',
          'xpathparser:9' => './/a/span[@class="bld lrg red"]',
        ),
        'rawXML' => array(
          'xpathparser:0' => 0,
          'xpathparser:1' => 0,
          'xpathparser:2' => 0,
          'xpathparser:3' => 0,
          'xpathparser:4' => 0,
          'xpathparser:5' => 0,
          'xpathparser:6' => 0,
          'xpathparser:7' => 0,
          'xpathparser:8' => 0,
          'xpathparser:9' => 0,
        ),
        'context' => '//div[@class="rsltGrid prod celwidget" or @class="fstRowGrid prod celwidget"]',
        'exp' => array(
          'errors' => 0,
          'debug' => array(
            'context' => 0,
            'xpathparser:0' => 0,
            'xpathparser:1' => 0,
            'xpathparser:2' => 0,
            'xpathparser:3' => 0,
            'xpathparser:4' => 0,
            'xpathparser:5' => 0,
            'xpathparser:6' => 0,
            'xpathparser:7' => 0,
            'xpathparser:8' => 0,
            'xpathparser:9' => 0,
          ),
        ),
        'allow_override' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'xpathparser:0',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'xpathparser:1',
            'target' => 'body',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'xpathparser:2',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'xpathparser:3',
            'target' => 'url',
            'unique' => 1,
          ),
          4 => array(
            'source' => 'xpathparser:4',
            'target' => 'Temporary target 1',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'xpathparser:5',
            'target' => 'field_csd_category_ref',
            'term_search' => '1',
            'autocreate' => 0,
          ),
          6 => array(
            'source' => 'xpathparser:6',
            'target' => 'Temporary target 2',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'xpathparser:7',
            'target' => 'Temporary target 3',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'xpathparser:8',
            'target' => 'Temporary target 4',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'xpathparser:9',
            'target' => 'field_csd_price2',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'csd_product',
      ),
    ),
    'content_type' => 'csd_amazon_import',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 1,
  );
  $export['csd_amazon_importer'] = $feeds_importer;

  return $export;
}
