<?php
/**
 * @file
 * csdrupal_amazon_parser.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function csdrupal_amazon_parser_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-csd_amazon_import-field_csd_category_ref'
  $field_instances['node-csd_amazon_import-field_csd_category_ref'] = array(
    'bundle' => 'csd_amazon_import',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'ul',
    'field_empty_text' => '',
    'field_name' => 'field_csd_category_ref',
    'label' => 'Category',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'content_taxonomy_opt_groups' => 0,
      ),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Category');

  return $field_instances;
}
