<?php
/**
 * @file
 * csdrupal_amazon_parser.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function csdrupal_amazon_parser_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function csdrupal_amazon_parser_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function csdrupal_amazon_parser_node_info() {
  $items = array(
    'csd_amazon_import' => array(
      'name' => t('CS Drupal: Amazon import'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
