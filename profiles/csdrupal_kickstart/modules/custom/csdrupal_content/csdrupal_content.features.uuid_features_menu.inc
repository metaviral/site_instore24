<?php
/**
 * @file
 * csdrupal_content.features.uuid_features_menu.inc
 */

/**
 * Implements hook_menu_default_uuid_features_menu().
 */
function csdrupal_content_menu_default_uuid_features_menu() {
  $menu_uuid_links = array();

  // Exported menu link: 17df551f-f81f-475b-b2aa-57c3a6b5ae6d
  $menu_uuid_links['17df551f-f81f-475b-b2aa-57c3a6b5ae6d'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'contact',
    'router_path' => 'contact',
    'link_title' => 'Contact us',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 30,
    'uuid' => '17df551f-f81f-475b-b2aa-57c3a6b5ae6d',
  );
  // Exported menu link: 7d661c79-3d91-455f-b642-22c1fbc11a0e
  $menu_uuid_links['7d661c79-3d91-455f-b642-22c1fbc11a0e'] = array(
    'menu_name' => 'menu-footer-menu',
    'router_path' => 'node/%',
    'link_title' => 'Privacy policy',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 10,
    'uuid' => '7d661c79-3d91-455f-b642-22c1fbc11a0e',
    'uuid_path' => 'node/d090d6b3-821b-4e47-8c8e-ed0177896b49',
  );
  // Exported menu link: 801ea7a3-bf59-4ce7-a346-29ef23bf9126
  $menu_uuid_links['801ea7a3-bf59-4ce7-a346-29ef23bf9126'] = array(
    'menu_name' => 'menu-footer-menu',
    'router_path' => 'node/%',
    'link_title' => 'Terms of Service',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 10,
    'uuid' => '801ea7a3-bf59-4ce7-a346-29ef23bf9126',
    'uuid_path' => 'node/2050f0fd-4d39-4ee2-911f-b7727c57d1c2',
  );
  // Exported menu link: f90e4c02-3a2c-4208-a9ab-d95448e41c2a
  $menu_uuid_links['f90e4c02-3a2c-4208-a9ab-d95448e41c2a'] = array(
    'menu_name' => 'menu-footer-menu',
    'router_path' => 'node/%',
    'link_title' => 'About us',
    'options' => array(
      'identifier' => 'menu-footer-menu_about-us:node/1866',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -20,
    'uuid' => 'f90e4c02-3a2c-4208-a9ab-d95448e41c2a',
    'uuid_path' => 'node/44af3932-f1de-426e-8e1c-56c88d39196e',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('About us');
  t('Contact us');
  t('Privacy policy');
  t('Terms of Service');

  return $menu_uuid_links;
}
