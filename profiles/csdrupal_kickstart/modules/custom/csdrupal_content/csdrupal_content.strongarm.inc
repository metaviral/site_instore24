<?php
/**
 * @file
 * csdrupal_content.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function csdrupal_content_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_entity_node';
  $strongarm->value = array(
    'page' => 'page',
    'csd_amazon_import' => 0,
    'csd_product' => 0,
  );
  $export['uuid_features_entity_node'] = $strongarm;

  return $export;
}
