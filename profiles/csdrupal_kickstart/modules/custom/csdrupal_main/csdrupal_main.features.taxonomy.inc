<?php
/**
 * @file
 * csdrupal_main.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function csdrupal_main_taxonomy_default_vocabularies() {
  return array(
    'csd_product_categories' => array(
      'name' => 'Product categories',
      'machine_name' => 'csd_product_categories',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'tags' => array(
      'name' => 'Tags',
      'machine_name' => 'tags',
      'description' => 'Use tags to group articles on similar topics into categories.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
