<?php
/**
 * @file
 * csdrupal_main.multifield.inc
 */

/**
 * Implements hook_multifield_default_multifield().
 */
function csdrupal_main_multifield_default_multifield() {
  $export = array();

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->api_version = 1;
  $multifield->machine_name = 'csd_customer_reviews';
  $multifield->label = 'Customer reviews';
  $multifield->description = '';
  $export['csd_customer_reviews'] = $multifield;

  return $export;
}
