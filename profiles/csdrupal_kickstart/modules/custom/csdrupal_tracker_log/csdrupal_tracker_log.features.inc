<?php
/**
 * @file
 * csdrupal_tracker_log.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function csdrupal_tracker_log_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
