<?php

function csdrupal_tracker_log_admin_log_report() {
  $show_user_agent = variable_get('csdrupal_tracker_log_report_show_user_agent', 0);

  $header = array(
    array('data' => t('ID'), 'field' => 'log.cslog_id', 'sort' => 'desc'),
    array('data' => t('Timestamp'), 'field' => 'log.timestamp'),
    array('data' => t('Redirect mode'), 'field' => 'log.redirect_mode'),
    array('data' => t('Path'), 'field' => 'log.path'),
    array('data' => t('Referrer'), 'field' => 'log.url'),
    array('data' => t('IP'), 'field' => 'log.hostname'),
    array('data' => t('ISP'), 'field' => 'log.isp_name'),
    array('data' => t('Country code'), 'field' => 'log.country_code'),
    array('data' => t('Region code'), 'field' => 'log.region_code'),
    array('data' => t('City'), 'field' => 'log.city'),
    // array('data' => t('Organization'), 'field' => 'log.organization_name'),
    array('data' => t('User agent'), 'field' => 'log.user_agent'),
  );

  if (!$show_user_agent) {
    array_pop($header);
  }

  // Log query
  $query = db_select('csdrupal_tracker_log', 'log', array('target' => 'slave'))->extend('PagerDefault')->extend('TableSort');
  $query->join('users', 'u', 'log.uid = u.uid');
  $query
    ->fields('log', 
      array(
        'cslog_id', 
        'timestamp', 
        'redirect_mode',
        'path', 
        'url', 
        'title', 
        'uid', 
        'hostname', 
        'isp_name',
        'country_code',
        'region_code',
        'city',
        // 'organization_name',
        'user_agent'
      ))
    ->fields('u', array('name'))
    ->limit(50)
    ->orderByHeader($header);

  $result = $query->execute();
  $rows = array();
  foreach ($result as $log) {
    $row = array(
      $log->cslog_id,
      array('data' => format_date($log->timestamp, 'short'), 'class' => array('nowrap')),
      $log->redirect_mode,
      $log->path,
      array('data' => $log->url, 'style' => 'word-wrap: break-word; max-width: 30em;'), // Referrer
      $log->hostname, // IP
      $log->isp_name, 
      $log->country_code,
      $log->region_code,
      $log->city,
      // $log->organization_name,
      $log->user_agent,
    );

    if (!$show_user_agent) {
      array_pop($row);
    }

    $rows[] = $row;
  }

  $build['statistics_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No statistics available.'),
  );

  $build['statistics_pager'] = array('#theme' => 'pager');

  return $build;
}
