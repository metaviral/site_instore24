<?php
/**
 * @file
 * csdrupal_tracker_log.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function csdrupal_tracker_log_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'smart_ip_auto_update';
  $strongarm->value = '1';
  $export['smart_ip_auto_update'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'smart_ip_debug';
  $strongarm->value = 0;
  $export['smart_ip_debug'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'smart_ip_ipinfodb_key';
  $strongarm->value = '';
  $export['smart_ip_ipinfodb_key'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'smart_ip_maxmind_bin_db_auto_update';
  $strongarm->value = '0';
  $export['smart_ip_maxmind_bin_db_auto_update'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'smart_ip_maxmind_key';
  $strongarm->value = 'O2uK9LqMajld';
  $export['smart_ip_maxmind_key'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'smart_ip_maxmind_license';
  $strongarm->value = 'O2uK9LqMajld';
  $export['smart_ip_maxmind_license'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'smart_ip_maxmind_service';
  $strongarm->value = 'city_isp_org';
  $export['smart_ip_maxmind_service'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'smart_ip_roles_to_geolocate';
  $strongarm->value = array(
    0 => 1,
    1 => 2,
    2 => 3,
  );
  $export['smart_ip_roles_to_geolocate'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'smart_ip_save_user_location_creation';
  $strongarm->value = 0;
  $export['smart_ip_save_user_location_creation'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'smart_ip_source';
  $strongarm->value = 'maxmindgeoip_service';
  $export['smart_ip_source'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'smart_ip_test_ip_address';
  $strongarm->value = '';
  $export['smart_ip_test_ip_address'] = $strongarm;

  return $export;
}
