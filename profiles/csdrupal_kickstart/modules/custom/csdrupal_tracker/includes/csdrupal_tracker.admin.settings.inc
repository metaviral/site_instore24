<?php

/**
 * CS Drupal Tracker settings page
 */
function csdrupal_admin_tracker_settings() {
  $form = array();

  $redirect_modes = array(
  	'off' => t('Redirect off'),
    // 'off_fix' => 'Off Fix',
  	'on' => t('Redirect on'),
  	'amztrk_redirect' => t('Redirect to Amztrk.com'),
  	// 'custom_redirect' => 'Custom redirect',
  );

  // Tracker settings
  $form['tracker'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tracker settings'),
    '#weight' => -10,
  );

  $form['tracker']['csdrupal_tracker_redirect_mode'] = array(
    '#type' => 'select',
    '#title' => t('Redirect mode'),
    #'#description' => t(''),
    '#options' => $redirect_modes,
    '#default_value' => variable_get('csdrupal_tracker_redirect_mode', 'off'),
  );

  $form['tracker']['csdrupal_tracker_fix_product'] = array(
    '#type' => 'textfield',
    '#title' => t('Fixed product'),
    '#description' => t('Choose fixed product (autocomplete field).'),
    '#default_value' => variable_get('csdrupal_tracker_fix_product', ''),
    // The autocomplete path is provided in hook_menu in ajax_example.module.
    '#autocomplete_path' => 'ajax/node_autocomplete_callback',
  );
  
  $form['tracker']['csdrupal_tracker_tracking_tag'] = array(
    '#type' => 'textfield',
    '#title' => t('Amazon tracking ID'),
    '#default_value' => variable_get('csdrupal_tracker_tracking_tag', ''),
    '#size' => 40,
    '#maxlength' => 100,
    '#description' => t('Example: tag=myamazontag.'),
    #'#required' => TRUE,
  );

  // Blacklist settings fieldset
  $form['blacklist'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tracker blacklist settings'),
  );

  // Blacklist
  $form['blacklist']['csdrupal_tracker_blacklist'] = array(
    '#type' => 'textarea',
    '#title' => t('Blacklist rules'),
    '#rows' => 20,
    '#default_value' => variable_get('csdrupal_tracker_blacklist', ''),
    '#description' => 
      t(
        "Seperate blacklist rules with newlines. Seperate multiple rules with '+' sign. Examples: <ul>
        <li><strong>ip:1.2.3.4</strong> or <strong>ip:1.2.3.*</strong> or <strong>ip:1.2.*.4</strong> (IP address)</li>
        <li><strong>ip:1.2.3.0-1.2.3.255</strong> (IP address with Start-End IP format)</li>
        <li><strong>ip:1.2.3/24</strong> (IP address with CIDR format)</li>
        <li><strong>ip:1.2.3.4/255.255.255.0</strong> (IP address with CIDR format)</li>
        <li><strong>isp:Amazon</strong> or <strong>isp:Amaz*</strong> (ISP name, ex. Facebook, Amazon)</li>
        <li><strong>cit:Paris</strong> or <strong>cit:Par*</strong> (City, ex. Paris, London, New York)</li>
        <li><strong>cou:US</strong> (Country code, ex. US, DE, RU)</li>
        <li><strong>reg:AZ</strong> (Region code, ex. AZ, MN, CA)</li>
        <li><strong>ip:1.2.3.4 + isp:Facebook + cou:US</strong> (Multiple rules. All criteria must pass.)</li>
        <li><strong>ip:1.2.3.* + isp:Facebook + cou:US + reg:AZ + cit:Paris</strong> (Multiple rules. All criteria must pass.)</li>
        <li><strong>ip:1.2.*.4 + isp:Face* + cou:US + reg:AZ + cit:Par*</strong> (Multiple rules. All criteria must pass.)</li>
        <li><strong>isp:Face* + cou:US + reg:? + cit:?</strong> (Multiple rules with empty parameters. All criteria must pass.)</li>
        </ul>"
      ),
  );
  
  return system_settings_form($form);
}

