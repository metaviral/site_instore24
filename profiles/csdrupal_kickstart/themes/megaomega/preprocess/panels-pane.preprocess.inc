<?php

/**
 * @file
 * Contains a pre-process hook for 'panels-pane'.
 */

/**
 * Implements hook_preprocess_panels_pane().
 */
function megaomega_preprocess_panels_pane(&$variables) { #dpm($variables);
	$pane = $variables['pane'];
	$replace_classes_array = array();

	ctools_include('cleanstring');

	// Replace pane title class
	$variables['title_attributes_array']['class'] = array_diff($variables['title_attributes_array']['class'], array('pane-title'));
	$variables['title_attributes_array']['class'][] = 'pane__title';

	// Replace classes arrays
  $type_class = $pane->type ? ctools_cleanstring($pane->type, array('lower case' => TRUE)) : '';
  $subtype_class = $pane->subtype ? ctools_cleanstring($pane->subtype, array('lower case' => TRUE)) : '';

  if (isset($type_class)) {
  	$replace_classes_array['pane-' . $type_class] = 'pane--' . $type_class;
  }

  if (isset($subtype_class)) {
  	$replace_classes_array['pane-' . $subtype_class] = 'pane--' . $subtype_class;
  }

  $replace_classes_array['panel-pane'] = 'pane';

  $variables['classes_array'] = str_replace(array_keys($replace_classes_array), array_values($replace_classes_array), $variables['classes_array']);
}

