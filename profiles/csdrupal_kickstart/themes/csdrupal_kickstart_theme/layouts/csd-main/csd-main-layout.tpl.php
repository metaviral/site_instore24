<div<?php print $attributes; ?>>

  <div id="top-wrapper">
    <header class="header" role="banner">
      <?php print render($page['header']); ?>
    </header>

    <div class="navigation">
      <?php print render($page['navigation']); ?>
    </div>
  </div> <!-- // top-wrapper -->

  <div id="main" class="main">
    <div class="content" role="main">
      <?php print render($page['highlighted']); ?>
      <?php print $breadcrumb; ?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </div>

    <?php // print render($page['sidebar_first']); ?>
    <?php // print render($page['sidebar_second']); ?>
  </div>

  <div id="footer-wrapper">
    <footer id="footer" class="footer" role="contentinfo">
      <?php print render($page['footer']); ?>
    </footer>
  </div>
</div>
