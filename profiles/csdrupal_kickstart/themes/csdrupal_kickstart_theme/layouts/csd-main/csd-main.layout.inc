name = CS Drupal Main
;description = Main layout for...
;preview = preview.png
template = csd-main-layout

; Regions
	;regions[branding]       = Branding
regions[header]         = Header
regions[navigation]     = Navigation bar
;regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[footer]         = Footer
