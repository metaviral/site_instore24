core = 7.x
api = 2


; Modules:

projects[uuid][type] = "module"
projects[uuid][version] = "1.x-dev"
projects[uuid][subdir] = "contrib"

projects[uuid_features][type] = "module"
projects[uuid_features][version] = "1.0-alpha4"
projects[uuid_features][subdir] = "contrib"

projects[uuid_features_menu][type] = "module"
projects[uuid_features_menu][subdir] = "contrib"
projects[uuid_features_menu][download][type] = "git"
projects[uuid_features_menu][download][url] = "http://git.drupal.org/sandbox/ioskevich/2163429.git"
projects[uuid_features_menu][download][branch] = "7.x-1.x"
	;projects[uuid_features_menu][download][revision] = "124b3c5739e1e476cac82313a6e1f7f647850066"

projects[field_extrawidgets][type] = "module"
projects[field_extrawidgets][version] = "1.1"
projects[field_extrawidgets][subdir] = "contrib"

projects[field_empty_text][type] = "module"
projects[field_empty_text][version] = "1.0-alpha3"
projects[field_empty_text][subdir] = "contrib"

projects[advagg][type] = "module"
projects[advagg][version] = "2.7"
projects[advagg][subdir] = "contrib"

projects[entitycache][type] = "module"
projects[entitycache][version] = "1.2"
projects[entitycache][subdir] = "contrib"

projects[fast_404][type] = "module"
projects[fast_404][version] = "1.x-dev"
projects[fast_404][subdir] = "contrib"

projects[filecache][type] = "module"
projects[filecache][version] = "1.x-dev"
projects[filecache][subdir] = "contrib"

projects[lazyload][type] = "module"
projects[lazyload][version] = "1.x-dev"
projects[lazyload][subdir] = "contrib"

projects[speedy][type] = "module"
projects[speedy][version] = "1.12"
projects[speedy][subdir] = "contrib"

projects[block_access][type] = "module"
projects[block_access][version] = "1.5"
projects[block_access][subdir] = "contrib"

projects[field_permissions][type] = "module"
projects[field_permissions][version] = "1.0-beta2"
projects[field_permissions][subdir] = "contrib"

projects[field_ui_permissions][type] = "module"
projects[field_ui_permissions][version] = "1.0-beta1"
projects[field_ui_permissions][subdir] = "contrib"

projects[menu_admin_per_menu][type] = "module"
projects[menu_admin_per_menu][version] = "1.0"
projects[menu_admin_per_menu][subdir] = "contrib"

projects[taxonomy_access_fix][type] = "module"
projects[taxonomy_access_fix][version] = "2.1"
projects[taxonomy_access_fix][subdir] = "contrib"

projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "3.2"

projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc4"

projects[backports][subdir] = "contrib"
projects[backports][version] = "1.0-alpha1"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.4"

projects[clean_markup][subdir] = "contrib"
projects[clean_markup][version] = "2.7"

projects[clean_module_list][subdir] = "contrib"
projects[clean_module_list][version] = "1.0"

projects[coffee][subdir] = "contrib"
projects[coffee][version] = "2.2"

projects[colorbox][subdir] = "contrib"
projects[colorbox][version] = "2.7"

projects[content_taxonomy][subdir] = "contrib"
projects[content_taxonomy][version] = "1.0-beta2"

projects[contextual_flyout_links][subdir] = "contrib"
projects[contextual_flyout_links][version] = "1.2"
projects[contextual_flyout_links][patches][] = "https://drupal.org/files/contextual_flyout_links-remove_div_selector-1479424-1479424.patch"

projects[currency][subdir] = "contrib"
projects[currency][version] = "2.4"

projects[date][subdir] = "contrib"
projects[date][version] = "2.8"

projects[defaultconfig][subdir] = "contrib"
projects[defaultconfig][version] = "1.0-alpha9"

projects[delete_orphaned_terms][subdir] = "contrib"
projects[delete_orphaned_terms][version] = "2.0-beta2"

projects[smart_ip][subdir] = "contrib"
projects[smart_ip][version] = "2.3"

projects[diff][subdir] = "contrib"
projects[diff][version] = "3.x-dev"

projects[ds][subdir] = "contrib"
projects[ds][version] = "2.6"

projects[elements][subdir] = "contrib"
projects[elements][version] = "1.4"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.5"

projects[entityreference][subdir] = "contrib"
projects[entityreference][version] = "1.1"

projects[facebook_boxes][subdir] = "contrib"
projects[facebook_boxes][version] = "1.0"

projects[facebook_comments][subdir] = "contrib"
projects[facebook_comments][version] = "1.x-dev"

projects[features][subdir] = "contrib"
projects[features][version] = "2.2"

projects[features_override][subdir] = "contrib"
projects[features_override][version] = "2.0-rc2"

projects[feeds][subdir] = "contrib"
projects[feeds][version] = "2.x-dev"

projects[feeds_crawler][subdir] = "contrib"
projects[feeds_crawler][version] = "2.x-dev"

projects[feeds_tamper][subdir] = "contrib"
projects[feeds_tamper][version] = "1.0"

projects[feeds_xpathparser][subdir] = "contrib"
projects[feeds_xpathparser][version] = "1.x-dev"

projects[fences][subdir] = "contrib"
projects[fences][version] = "1.x-dev"
projects[fences][patches][] = "https://bitbucket.org/dinamicodev/drupal/downloads/field_for_wrapper_css_class-1679684-8.patch"

projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.4"

projects[follow][subdir] = "contrib"
projects[follow][version] = "2.x-dev"

projects[panels][subdir] = "contrib"
projects[panels][version] = "3.4"

projects[imagecache_actions][subdir] = "contrib"
projects[imagecache_actions][version] = "1.x-dev"

projects[instantfilter][subdir] = "contrib"
projects[instantfilter][version] = "1.x-dev"

projects[job_scheduler][subdir] = "contrib"
projects[job_scheduler][version] = "2.x-dev"

projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "2.4"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.2"

projects[menu_block][subdir] = "contrib"
projects[menu_block][version] = "2.x-dev"

projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "2.0-alpha2"

projects[money][subdir] = "contrib"
projects[money][version] = "1.x-dev"

projects[multifield][type] = "module"
projects[multifield][subdir] = "contrib"
projects[multifield][download][type] = "file"
projects[multifield][download][url] = "https://bitbucket.org/dinamicodev/drupal/downloads/multifield-7.x-1.0-unstable9-17-dev-patch-2102265-12.zip"

projects[multiform][subdir] = "contrib"
projects[multiform][version] = "1.1"

projects[references][subdir] = "contrib"
projects[references][version] = "2.1"

projects[panelizer][subdir] = "contrib"
projects[panelizer][version] = "3.x-dev"

projects[panels_everywhere][subdir] = "contrib"
projects[panels_everywhere][version] = "1.x-dev"

projects[panels_extra_styles][subdir] = "contrib"
projects[panels_extra_styles][version] = "1.x-dev"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.x-dev"

projects[pathauto_live_preview][type] = "module"
projects[pathauto_live_preview][subdir] = "contrib"
projects[pathauto_live_preview][download][type] = "file"
projects[pathauto_live_preview][download][url] = "https://bitbucket.org/dinamicodev/drupal/downloads/drupalgardens_pathauto_live_preview.tar.gz"

projects[pathauto_persist][subdir] = "contrib"
projects[pathauto_persist][version] = "1.3"

projects[plupload][subdir] = "contrib"
projects[plupload][version] = "1.6"

projects[radioactivity][subdir] = "contrib"
projects[radioactivity][version] = "2.9"

projects[search_api][subdir] = "contrib"
projects[search_api][version] = "1.13"

projects[search_api_db][subdir] = "contrib"
projects[search_api_db][version] = "1.3"

projects[search_api_page][subdir] = "contrib"
projects[search_api_page][version] = "1.1"

projects[semantic_panels][subdir] = "contrib"
projects[semantic_panels][version] = "1.2"

projects[semanticviews][subdir] = "contrib"
projects[semanticviews][version] = "1.0-alpha1"

projects[sharethis][subdir] = "contrib"
projects[sharethis][version] = "2.9"

projects[similar][subdir] = "contrib"
projects[similar][version] = "2.0-beta5"

projects[similarterms][subdir] = "contrib"
projects[similarterms][version] = "2.3"
projects[similarterms][patch][] = "https://www.drupal.org/files/0001-similarterm-options-were-not-being-saved-due-to-wron.patch"

projects[simplified_menu_admin][subdir] = "contrib"
projects[simplified_menu_admin][version] = "1.x-dev"

projects[smart_trim][subdir] = "contrib"
projects[smart_trim][version] = "1.4"

projects[special_menu_items][subdir] = "contrib"
projects[special_menu_items][version] = "2.0"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.x-dev"

projects[tagclouds][subdir] = "contrib"
projects[tagclouds][version] = "1.10"

projects[taxonomy_breadcrumb][subdir] = "contrib"
projects[taxonomy_breadcrumb][version] = "1.x-dev"

projects[taxonomy_menu][subdir] = "contrib"
projects[taxonomy_menu][version] = "2.x-dev"

projects[title][subdir] = "contrib"
projects[title][version] = "1.x-dev"

projects[token][subdir] = "contrib"
projects[token][version] = "1.x-dev"

projects[transliteration][subdir] = "contrib"
projects[transliteration][version] = "3.x-dev"

projects[views][subdir] = "contrib"
projects[views][version] = "3.8"

; Themes:
projects[omega][version] = "4.2"


; Libraries:
  ;libraries[backbone][download][type] = "get"
  ;libraries[backbone][type] = "libraries"
  ;libraries[backbone][download][url] = "https://github.com/jashkenas/backbone/archive/1.1.0.tar.gz"

libraries[ckeditor][download][type] = get
libraries[ckeditor][type] = "libraries"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor%20for%20Drupal/edit/ckeditor_4.3.2_edit.zip"

libraries[colorbox][download][type] = "get"
libraries[colorbox][type] = "libraries"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/master.zip"

  ;libraries[jquery.bxslider][type] = "libraries"
  ;libraries[jquery.bxslider][download][type] = "file"
  ;libraries[jquery.bxslider][download][url] = "https://github.com/wandoledzep/bxslider-4/archive/master.zip"

  ;libraries[jquery_expander][type] = "libraries"
  ;libraries[jquery_expander][download][type] = "file"
  ;libraries[jquery_expander][download][url] = "https://github.com/kswedberg/jquery-expander/archive/master.zip"

libraries[modernizr][download][type] = "get"
libraries[modernizr][type] = "libraries"
libraries[modernizr][download][url] = "https://github.com/Modernizr/Modernizr/archive/v2.7.1.tar.gz"

libraries[plupload][download][type] = "get"
libraries[plupload][type] = "libraries"
libraries[plupload][download][url] = "https://github.com/moxiecode/plupload/archive/v1.5.8.zip"
libraries[plupload][patch][1903850] = "http://drupal.org/files/issues/plupload-1_5_8-rm_examples-1903850-16.patch"

  ;libraries[timeago][download][type] = "get"
  ;libraries[timeago][type] = "libraries"
  ;libraries[timeago][download][url] = "https://github.com/rmm5t/jquery-timeago/archive/v1.4.1.zip"

  ;libraries[underscore][download][type] = "get"
  ;libraries[underscore][type] = "libraries"
  ;libraries[underscore][download][url] = "https://github.com/jashkenas/underscore/archive/1.5.2.zip"

  ;libraries[imagesloaded][download][type] = "get"
  ;libraries[imagesloaded][type] = "libraries"
  ;libraries[imagesloaded][download][url] = "https://github.com/desandro/imagesloaded/archive/v2.1.2.tar.gz"

  ;libraries[imgareaselect][download][type] = "get"
  ;libraries[imgareaselect][type] = "libraries"
  ;libraries[imgareaselect][download][url] = "http://odyniec.net/projects/imgareaselect/jquery.imgareaselect-0.9.10.zip"
